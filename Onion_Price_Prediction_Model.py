#!/usr/bin/env python
# coding: utf-8

# # Onion Price Predication Model using Seaborn
# #By- Aarush Kumar
# #Dated: September 05,2021

# In[1]:


import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns


# In[2]:


df=pd.read_csv(r'/home/aarush100616/Downloads/Projects/Onion Price Prediction/Onion Prices 2020.csv')


# In[3]:


df


# ## EDA

# In[4]:


df.info()


# In[5]:


df.isnull().sum()


# In[6]:


df.shape


# In[7]:


df.size


# In[8]:


df.dtypes


# ## Visualization

# In[9]:


sns.countplot(x='variety',data=df)


# In[10]:


sns.barplot(df['variety'],df['modal_price'])


# In[11]:


df=pd.get_dummies(df)


# In[12]:


df.head()


# In[13]:


x =df.loc[:,df.columns!='modal_price']


# In[14]:


x.shape


# In[15]:


y= df['modal_price']


# In[16]:


y.shape


# In[17]:


from sklearn.model_selection import train_test_split


# In[18]:


x_train,x_test,y_train,y_test=train_test_split(x,y,test_size=0.2,random_state=0)


# In[19]:


x_train


# In[20]:


x_test


# In[21]:


y_train


# In[22]:


y_test


# In[23]:


from sklearn.linear_model import LinearRegression


# In[24]:


model=LinearRegression().fit(x_train,y_train)


# In[25]:


model.score(x_test,y_test)*100


# In[26]:


model.predict(x_test)


# In[27]:


y_pred=model.predict(x_test)


# In[28]:


y_pred = model.predict(x)
print('predicted response:', y_pred, sep='\n')

